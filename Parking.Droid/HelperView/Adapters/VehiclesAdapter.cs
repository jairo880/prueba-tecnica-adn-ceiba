﻿using System;
using System.Collections.Generic;
using Android.Support.V7.Widget;
using Parking.Core.BussinessModel;
using System.Linq;
using Android.Views;
using Parking.Droid.Helper.ViewHolder;
using Parking.Core.Resources;
using System.Globalization;

namespace Parking.Droid.Helper
{
    public class VehiclesAdapter : RecyclerView.Adapter
    {
        public event EventHandler<Vehicle> ItemClick;

        private IEnumerable<Vehicle> vehicles;

        public VehiclesAdapter(IEnumerable<Vehicle> vehicles)
        {
            this.vehicles = vehicles;
        }

        public override int ItemCount => vehicles.Count();

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            VehicleViewHolder viewHolder = holder as VehicleViewHolder;

            ConfigureTittleLabelsItem(viewHolder);
            FillInformationItem(viewHolder, vehicles.ElementAt(position));

        }

        private void ConfigureTittleLabelsItem(VehicleViewHolder viewHolder)
        {
            viewHolder.TittlePlate.Text = ParkingTexts.tittle_plate_;
            viewHolder.TittleEngine.Text = ParkingTexts.tittle_engine_;
            viewHolder.TittleType.Text = ParkingTexts.tittle_type_;
            viewHolder.TittleDateIn.Text = ParkingTexts.tittle_date_in_;
        }

        private void FillInformationItem(VehicleViewHolder viewHolder, Vehicle vehicle)
        {
            viewHolder.Plate.Text = vehicle.Plate;
            viewHolder.Engine.Text = vehicle.EngineCC.ToString(CultureInfo.InvariantCulture);
            viewHolder.Type.Text = vehicle.Type == Core.Resources.Enums.VehicleTypes.Motorcycle ? ParkingTexts.motorcycle : ParkingTexts.car;
            viewHolder.DateIn.Text = vehicle.AdmisionVehicleDate.ToString(CultureInfo.InvariantCulture);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.vehicle_card_view, parent, false);
            VehicleViewHolder viewHolder = new VehicleViewHolder(view, OnItemClick);

            return viewHolder;
        }

        public void UpdateData(IEnumerable<Vehicle> vehicles)
        {
            this.vehicles = vehicles;
            NotifyDataSetChanged();
        }

        void OnItemClick(int position)
        {
            ItemClick?.Invoke(this, vehicles.ElementAt(position));
        }


    }
}
