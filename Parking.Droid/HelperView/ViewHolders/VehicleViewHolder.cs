﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace Parking.Droid.Helper.ViewHolder
{
    public class VehicleViewHolder : RecyclerView.ViewHolder
    {
        public TextView TittlePlate { get; private set; }
        public TextView TittleEngine { get; private set; }
        public TextView TittleType { get; private set; }
        public TextView TittleDateIn { get; private set; }
        public TextView Plate { get; private set; }
        public TextView Engine { get; private set; }
        public TextView Type { get; private set; }
        public TextView DateIn { get; private set; }
        public LinearLayout ContentVehicle { get; private set; }

        public VehicleViewHolder(View itemView, Action<int> listener) : base(itemView)
        {
            TittlePlate = itemView.FindViewById<TextView>(Resource.Id.text_view_tittle_vehicle_plate_card_view);
            TittleEngine = itemView.FindViewById<TextView>(Resource.Id.text_view_tittle_engine_type_in_card_view);
            TittleType = itemView.FindViewById<TextView>(Resource.Id.text_view_tittle_vehicle_type_in_card_view);
            TittleDateIn = itemView.FindViewById<TextView>(Resource.Id.text_view_tittle_vehicle_date_in_card_view);
            Plate = itemView.FindViewById<TextView>(Resource.Id.text_view_vehicle_plate_card_view);
            Engine = itemView.FindViewById<TextView>(Resource.Id.text_view_vehicle_engine_card_view);
            Type = itemView.FindViewById<TextView>(Resource.Id.text_view_vehicle_type_card_view);
            DateIn = itemView.FindViewById<TextView>(Resource.Id.text_view_vehicle_date_in_card_view);

            ContentVehicle = itemView.FindViewById<LinearLayout>(Resource.Id.layout_content_vehicle_card_view);

            itemView.Click += (sender, e) =>
            {
                listener(LayoutPosition);
                ContentVehicle.SetBackgroundResource(Resource.Drawable.ripple_effect);
            };


        }
    }
}
