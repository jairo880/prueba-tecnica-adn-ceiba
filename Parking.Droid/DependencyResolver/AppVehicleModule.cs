﻿using Ninject.Modules;
using Parking.Core.Interfaces;
using Parking.Core;
using Parking.Core.Domain.ExitVehicle;
using Parking.Droid.Implementation.Vehicles;

namespace Parking.Droid.DependencyResolver
{
    public class AppVehicleModule : NinjectModule
    {
        public override void Load()
        {
            this.Bind<IBussinessLogicVehicle>().To<BussinessLogicVehicle>().InSingletonScope();
            this.Bind<IViewVehicle>().To<VehicleActivity>().InSingletonScope();
            this.Bind<IBussinessLogicVehicleExit>().To<BussinessLogicVehicleExit>().InSingletonScope();
        }
    }
}
