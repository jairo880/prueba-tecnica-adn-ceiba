﻿using System;
using System.Reflection;
using Ninject;
using Parking.Core.Interfaces;
using Ninject.Parameters;
using Java.Lang.Reflect;

namespace Parking.Droid.DependencyResolver
{
    public static class ServiceLocatorApp
    {
        private static IKernel kernel;

        public static void ConfigureKernel()
        {

            if (kernel == null)
            {
                var settings = new NinjectSettings() { LoadExtensions = false };
                kernel = new StandardKernel(settings);
                kernel.Load(Assembly.GetExecutingAssembly());
            }
        }

        public static T Get<T>()
        {
            if (kernel == null)
            {
                throw new InvalidOperationException();
            }

            return kernel.Get<T>();

        }

        public static T Get<T>(ConstructorArgument argument)
        {
            if (kernel == null)
            {
                throw new InvalidOperationException();
            }

            return kernel.Get<T>(argument);
        }

    }
}
