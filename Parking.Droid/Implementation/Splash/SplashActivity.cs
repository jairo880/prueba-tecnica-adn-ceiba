﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Parking.Core.DependencyResolver;
using Parking.Droid.DependencyResolver;
using Android.Widget;
using Android.Views.Animations;
using Android.Content;
using Parking.Droid.Implementation.Vehicles;
using Parking.Core.Resources;

namespace Parking.Droid
{
    [Activity(Theme = "@style/AppThemeActionBarDisable", MainLauncher = true)]
    public class SplashActivity : AppCompatActivity
    {
        private ImageView imageViewLogo;
        private TextView textViewTittleADN;
        private TextView textViewTittleParking;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.splash_activity);

            ConfigureDependencyInjection();

            imageViewLogo = FindViewById<ImageView>(Resource.Id.image_view_logo);
            textViewTittleADN = FindViewById<TextView>(Resource.Id.tittle_adn_splash);
            textViewTittleParking = FindViewById<TextView>(Resource.Id.tittle_parking_splash);

            ConfigureTittleLabelsView();
            AnimateLogInit();
        }

        private void ConfigureTittleLabelsView()
        {
            textViewTittleADN.Text = ParkingTexts.tittle_ADN;
            textViewTittleParking.Text = ParkingTexts.tittle__parking;
        }

        private static void ConfigureDependencyInjection()
        {
            ServiceLocator.ConfigureKernel();
            ServiceLocatorApp.ConfigureKernel();
        }

        private void AnimateLogInit()
        {
            Animation animation = AnimationUtils.LoadAnimation(this, Resource.Animation.animationtest);
            animation.AnimationEnd += delegate
            {
                StartActivity(new Intent(this, typeof(VehicleActivity)));
            };

            imageViewLogo.StartAnimation(animation);

        }

    }
}