﻿using Android.App;
using Android.OS;
using Android.Widget;
using Newtonsoft.Json;
using Parking.Core.BussinessModel;
using Android.Support.V7.App;
using Parking.Core.Interfaces;
using Parking.Core.Domain.ExitVehicle;
using Android.Views;
using Parking.Droid.DependencyResolver;
using Parking.Core.Resources;
using System;
using Parking.Core.Resources.Enums;

namespace Parking.Droid.Implementation.VehicleDetail
{
    [Activity(Theme = "@style/AppThemeActionBarDisable")]
    public class VehicleDetailActivity : AppCompatActivity
    {
        private Vehicle vehicle;
        private TextView textViewTittlePlate;
        private TextView textViewTittleEngine;
        private TextView textViewTittleType;
        private TextView textViewTittleDateIn;
        private TextView textViewTittleHours;
        private TextView textViewTittleDays;
        private TextView textViewTittleTotalCostToPay;
        private TextView textViewTittleOutVehicleButton;
        private TextView textViewPlate;
        private TextView textViewEngine;
        private TextView textViewType;
        private TextView textViewDateIn;
        private TextView textViewHours;
        private TextView textViewDays;
        private TextView textViewTotalCostToPay;
        private LinearLayout buttonOutVehicle;
        private IBussinessLogicVehicleExit bussinessLogicVehicleExit;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.vehicle_detail_activity);

            bussinessLogicVehicleExit = ServiceLocatorApp.Get<IBussinessLogicVehicleExit>();

            textViewTittleHours = FindViewById<TextView>(Resource.Id.tittle_text_view_hours);
            textViewTittleDays = FindViewById<TextView>(Resource.Id.tittle_text_view_days);
            textViewTittleTotalCostToPay = FindViewById<TextView>(Resource.Id.tittle_text_view_cost_to_pay_parking);
            textViewTittleOutVehicleButton = FindViewById<TextView>(Resource.Id.tittle_text_view_out_vehicle);
            textViewTittlePlate = FindViewById<TextView>(Resource.Id.text_view_tittle_vehicle_plate_card_view);
            textViewTittleEngine = FindViewById<TextView>(Resource.Id.text_view_tittle_engine_type_in_card_view);
            textViewTittleType = FindViewById<TextView>(Resource.Id.text_view_tittle_vehicle_type_in_card_view);
            textViewTittleDateIn = FindViewById<TextView>(Resource.Id.text_view_tittle_vehicle_date_in_card_view);
            textViewPlate = FindViewById<TextView>(Resource.Id.text_view_vehicle_plate_card_view);
            textViewEngine = FindViewById<TextView>(Resource.Id.text_view_vehicle_engine_card_view);
            textViewType = FindViewById<TextView>(Resource.Id.text_view_vehicle_type_card_view);
            textViewDateIn = FindViewById<TextView>(Resource.Id.text_view_vehicle_date_in_card_view);
            textViewHours = FindViewById<TextView>(Resource.Id.text_view_hours);
            textViewDays = FindViewById<TextView>(Resource.Id.text_view_days);
            textViewTotalCostToPay = FindViewById<TextView>(Resource.Id.text_view_cost_to_pay_parking);
            buttonOutVehicle = FindViewById<LinearLayout>(Resource.Id.button_out_vehicle);

            GetVehicle();
            ConfigureLabels();
            FillInformationHeaderVehicle();
            CalculateCostToPay();
            GetHoursCarInsideParking();
            GetDaysCarInsideParkging();
            AddBackButtonEnabled();
            AddListeners();
        }



        private void ConfigureLabels()
        {
            textViewTittlePlate.Text = ParkingTexts.tittle_plate_;
            textViewTittleEngine.Text = ParkingTexts.tittle_engine_;
            textViewTittleType.Text = ParkingTexts.tittle_type_;
            textViewTittleDateIn.Text = ParkingTexts.tittle_date_in_;
            textViewTittleHours.Text = ParkingTexts.tittle_total_hours_;
            textViewTittleDays.Text = ParkingTexts.tittle_total_days_;
            textViewTittleTotalCostToPay.Text = ParkingTexts.tittle_cost_to_pay_parking_;
            textViewTittleOutVehicleButton.Text = ParkingTexts.tittle_out_vehicle;
        }


        private void AddBackButtonEnabled()
        {
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        private void GetVehicle()
        {
            vehicle = JsonConvert.DeserializeObject<Vehicle>(Intent.GetStringExtra("Vehicle"));
        }

        private void AddListeners()
        {
            buttonOutVehicle.Click += delegate
            {
                VehicleExit();
            };
        }

        private void VehicleExit()
        {
            bussinessLogicVehicleExit.VehicleExit(vehicle);
            Toast.MakeText(this, ParkingTexts.message_vehicle_exit_succesfully, ToastLength.Short).Show();
            Finish();
        }

        private void FillInformationHeaderVehicle()
        {
            if (vehicle == null)
                return;

            textViewPlate.Text = vehicle.Plate;
            textViewEngine.Text = vehicle.EngineCC.ToString();
            textViewType.Text = vehicle.Type == VehicleTypes.Motorcycle ? ParkingTexts.motorcycle : ParkingTexts.car;
            textViewDateIn.Text = vehicle.AdmisionVehicleDate.ToString();

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                default:
                    OnBackPressed();
                    return true;
            }
        }

        [Android.Runtime.Register("onBackPressed", "()V", "GetOnBackPressedHandler")]
        public virtual void OnBackPressed()
        {
            base.OnBackPressed();
        }

        private void CalculateCostToPay()
        {
            textViewTotalCostToPay.Text = bussinessLogicVehicleExit.GetTotalCostToPay(vehicle).ToString();
        }

        private void GetDaysCarInsideParkging()
        {
            textViewDays.Text = bussinessLogicVehicleExit.GetDaysVehicleInsideParking(vehicle).ToString();
        }

        private void GetHoursCarInsideParking()
        {
            textViewHours.Text = bussinessLogicVehicleExit.GetHoursVehicleInsideParking(vehicle).ToString();
        }
    }
}
