﻿namespace Parking.Droid.Implementation.Vehicles.Contract
{
    public interface IPresenterVehicle
    {
        void ValidatePlateEnabledDayToIncomeVehicle(string plate);
    }
}
