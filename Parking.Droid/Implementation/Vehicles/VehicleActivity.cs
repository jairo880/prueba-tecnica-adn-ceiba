﻿using Android.App;
using Android.OS;
using Android.Widget;
using Android.Support.V7.App;
using Refractored.Fab;
using Android.Views;
using System;
using Parking.Core.Interfaces;
using Android.Support.V7.Widget;
using Parking.Droid.Helper;
using Parking.Core.BussinessModel;
using Android.Content;
using Parking.Droid.Implementation.VehicleDetail;
using Newtonsoft.Json;
using Parking.Droid.DependencyResolver;
using Ninject.Parameters;
using Parking.Core.Resources;

namespace Parking.Droid.Implementation.Vehicles
{
    [Activity(Theme = "@style/AppThemeActionBarDisable")]
    public class VehicleActivity : AppCompatActivity, IViewVehicle
    {

        private FloatingActionButton floatingActionButtonAddVehicleParkingLot;
        private RecyclerView recyclerView;
        private IBussinessLogicVehicle bussinessLogicVehicle;
        private VehiclesAdapter vehiclesAdapter;
        private Android.App.AlertDialog alertDialog;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.vehicle_activity);

            bussinessLogicVehicle = ServiceLocatorApp.Get<IBussinessLogicVehicle>(new ConstructorArgument("View", this));

            floatingActionButtonAddVehicleParkingLot = FindViewById<FloatingActionButton>(Resource.Id.floating_action_button_add_vehicle_parking);
            recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerViewVehicles);

            AddListeners();

        }

        private void GetVehiclesIncomeParkingLot()
        {
            var vehicles = bussinessLogicVehicle.GetVehiclesInParkingLot();
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.SetLayoutManager(mLayoutManager);
            vehiclesAdapter = new VehiclesAdapter(vehicles);
            vehiclesAdapter.ItemClick += OnItemClickVehicleList;
            recyclerView.SetAdapter(vehiclesAdapter);
        }

        private void RefreshVehicleList()
        {
            vehiclesAdapter.UpdateData(bussinessLogicVehicle.GetVehiclesInParkingLot());
        }

        private void AddListeners()
        {
            floatingActionButtonAddVehicleParkingLot.Click += delegate
            {
                ShowDialogFragmentAddPlate();
            };
        }

        private void ShowDialogFragmentAddPlate()
        {

            if (!ValidateAlertDialogIsNull())
            {
                if (alertDialog.IsShowing)
                {
                    return;
                }
            }

            View view = LayoutInflater.Inflate(Resource.Layout.DialogFragmentAddPlate, null);
            alertDialog = new Android.App.AlertDialog.Builder(this).Create();
            alertDialog.SetTitle(ParkingTexts.tittle_vehicle_plate);
            alertDialog.SetView(view);
            alertDialog.SetCanceledOnTouchOutside(true);

            TextView buttonIncomeVehicle = view.FindViewById<TextView>(Resource.Id.button_income_vehicle_dialog);
            TextView buttonCancelDialog = view.FindViewById<TextView>(Resource.Id.button_cancel_dialog_income_vehicle);
            EditText editTextPlateVehicle = view.FindViewById<EditText>(Resource.Id.edit_text_plate);

            buttonIncomeVehicle.Text = ParkingTexts.tittle_income_vehicle_action;
            buttonCancelDialog.Text = ParkingTexts.tittle_cancel;
            editTextPlateVehicle.Hint = ParkingTexts.tittle_add_plate_vehicle;

            buttonIncomeVehicle.Click += delegate
            {
                alertDialog.Dismiss();
                if (bussinessLogicVehicle.ValidatePlateIsValid(editTextPlateVehicle.Text))
                {
                    ValidatePlateToIncomeRequest(editTextPlateVehicle.Text);
                    return;
                }

                Toast.MakeText(this, ParkingTexts.message_plate_invalid, ToastLength.Short).Show();
            };

            buttonCancelDialog.Click += delegate
            {
                alertDialog.Dismiss();
            };

            alertDialog.Show();
        }

        private void ShowDialogFragmentAddNewVehicle(string plate)
        {
            if (!ValidateAlertDialogIsNull())
            {
                if (alertDialog.IsShowing)
                {
                    return;
                }
            }

            View view = LayoutInflater.Inflate(Resource.Layout.DialogFragmentAddNewVehicle, null);
            alertDialog = new Android.App.AlertDialog.Builder(this).Create();
            alertDialog.SetTitle(ParkingTexts.tittle_new_vehicle);
            alertDialog.SetView(view);
            alertDialog.SetCanceledOnTouchOutside(true);

            TextView buttonIncomeVehicle = view.FindViewById<TextView>(Resource.Id.button_income_dialog_new_vehicle);
            TextView buttonCancelDialog = view.FindViewById<TextView>(Resource.Id.button_cancel_dialog_dialog_new_vehicle);
            EditText editTextPlateVehicle = view.FindViewById<EditText>(Resource.Id.edit_text_plate_new_vehicle_dialog);
            EditText editTextEngineVehicle = view.FindViewById<EditText>(Resource.Id.edit_text_engine_new_vehicle_dialog);
            Spinner spinner = view.FindViewById<Spinner>(Resource.Id.spinner_type_new_vehicle_dialog);

            buttonIncomeVehicle.Text = ParkingTexts.tittle_income_vehicle_action;
            buttonCancelDialog.Text = ParkingTexts.tittle_cancel;
            editTextEngineVehicle.Hint = ParkingTexts.tittle_add_engine_vehicle;
            editTextPlateVehicle.Hint = ParkingTexts.tittle_add_plate_vehicle;

            editTextPlateVehicle.Text = plate;

            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.vehicle_types, Android.Resource.Layout.SimpleSpinnerItem);

            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            buttonIncomeVehicle.Click += delegate
            {
                alertDialog.Dismiss();
                ValidateFormNewVehicleToIncome(editTextPlateVehicle.Text, spinner.SelectedItemPosition, editTextEngineVehicle.Text);

            };

            buttonCancelDialog.Click += delegate
            {
                alertDialog.Dismiss();
            };

            alertDialog.Show();
        }

        private bool ValidateAlertDialogIsNull() => alertDialog == null;

        private void ValidateFormNewVehicleToIncome(string plate, int selectedItemPosition, string engine)
        {
            if (string.IsNullOrEmpty(plate))
            {
                Toast.MakeText(this, ParkingTexts.message_plate_required, ToastLength.Short).Show();
                return;
            }

            if (string.IsNullOrEmpty(engine))
            {
                Toast.MakeText(this, ParkingTexts.message_engine_required, ToastLength.Short).Show();
                return;
            }

            if (selectedItemPosition == Convert.ToInt32(ParkingTexts.zero))
            {
                Toast.MakeText(this, ParkingTexts.message_select_type_vehicle_to_income, ToastLength.Short).Show();
                return;
            }

            RequestIncomeVehicle(plate, selectedItemPosition, engine);
        }

        private void RequestIncomeVehicle(string plate, int selectedItemPosition, string engine)
        {
            bussinessLogicVehicle.AddVehicleDB(bussinessLogicVehicle.
                BuildVehicleToIncomeParking(plate, selectedItemPosition == Convert.ToInt32(ParkingTexts.typeMotorcycle) ? Core.Resources.Enums.VehicleTypes.Motorcycle :
                Core.Resources.Enums.VehicleTypes.Car, Int32.Parse(engine)));
        }

        private void ValidatePlateToIncomeRequest(string plate)
        {
            if (string.IsNullOrEmpty(plate))
            {
                Toast.MakeText(this, ParkingTexts.message_plate_required, ToastLength.Short).Show();
                return;
            }

            bussinessLogicVehicle.ValidatePlateEnabledDayToIncomeVehicle(plate);
        }

        private void OnItemClickVehicleList(object sender, Vehicle vehicle)
        {
            var intent = new Intent(this, typeof(VehicleDetailActivity));
            intent.PutExtra("Vehicle", JsonConvert.SerializeObject(vehicle));
            StartActivity(intent);
        }

        public void ShowMessage(string message) => Toast.MakeText(this, message, ToastLength.Short).Show();

        public void ShowMessageError(string message) => Toast.MakeText(this, message, ToastLength.Short).Show();

        public void OnVehicleDontExistOnDatabase(string plate) => ShowDialogFragmentAddNewVehicle(plate);

        public void VehicleWasIncomeSuccessfully(string message)
        {
            RefreshVehicleList();
            Toast.MakeText(this, message, ToastLength.Short).Show();
        }

        protected override void OnResume()
        {
            base.OnResume();

            ValidateContentVehicles();
        }

        private void ValidateContentVehicles()
        {
            if (vehiclesAdapter == null)
            {
                GetVehiclesIncomeParkingLot();
                return;
            }

            RefreshVehicleList();
        }
    }
}
