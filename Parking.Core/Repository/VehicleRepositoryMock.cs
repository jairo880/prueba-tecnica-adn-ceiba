﻿using System;
using System.Collections.Generic;
using Parking.Core.BussinessModel;
using Parking.Core.Interfaces;
using Parking.Core.Utils;

namespace Parking.Core.Repository
{
    public class VehicleRepositoryMock : IRepositoryVehicle
    {

        private readonly List<Vehicle> vehicles = new List<Vehicle>();

        public VehicleRepositoryMock()
        {
            AddVehicleDB(new Vehicle
            {
                Plate = "ABC236",
                EngineCC = 1600,
                AdmisionVehicleDate = DateTimeManager.GetDateFromString("2019-04-05 06:12 AM"),
                DepartureVehicleDate = DateTimeManager.getDateTimeNow(),
                Type = Resources.Enums.VehicleTypes.Car,
                Status = Resources.Enums.VehicleStatus.OutParkingLot
            });
        }

        public void AddVehicleDB(Vehicle vehicle) => vehicles.Add(vehicle);

        public List<Vehicle> GetAllVehiclesRegistered() => vehicles;

        public Vehicle GetVehicleByPlate(string plate)
        {
            Vehicle vehicleFinal = null;

            foreach (Vehicle vehicle in vehicles)
            {
                if (vehicle.Plate.Equals(plate))
                {
                    vehicleFinal = vehicle;
                    break;
                }
            }

            return vehicleFinal;
        }

        public List<Vehicle> GetVehiclesInParkingLot()
        {
            return new List<Vehicle>();
        }

        public void UpdateVehicle(Vehicle vehicle)
        {

        }

        public bool ValidateVehicleAlreadyExistOnDB(string plate)
        {
            bool result = false;

            foreach (Vehicle vehicle in vehicles)
            {
                if (vehicle.Plate.Equals(plate))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }
    }
}
