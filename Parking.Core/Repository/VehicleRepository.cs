﻿using System.Collections.Generic;
using Parking.Core.BussinessModel;
using Parking.Core.Interfaces;
using Parking.Core.Utils;
using Parking.Core.DependencyResolver;

namespace Parking.Core.Repository
{
    public class VehicleRepository : IRepositoryVehicle
    {
        private readonly ITransactionRequestDBManager transactionRequestDBManager;

        public VehicleRepository()
        {
            transactionRequestDBManager = ServiceLocator.Get<ITransactionRequestDBManager>();
        }

        public void AddVehicleDB(Vehicle vehicle)
        {
            transactionRequestDBManager.AddVehicle(vehicle);
        }

        public List<Vehicle> GetAllVehiclesRegistered() =>
            transactionRequestDBManager.GetAllVehicles();


        public Vehicle GetVehicleByPlate(string plate) =>
            transactionRequestDBManager.GetVehicleByPlate(plate);

        public List<Vehicle> GetVehiclesInParkingLot() => transactionRequestDBManager.GetAllVehiclesCurrentlyParkingLot();

        public void UpdateVehicle(Vehicle vehicle)
        {
            transactionRequestDBManager.UpdateVehicle(vehicle);
        }

        public bool ValidateVehicleAlreadyExistOnDB(string plate) =>
            transactionRequestDBManager.ValidateVehicleAlreadyExistOnDB(plate);
    }
}
