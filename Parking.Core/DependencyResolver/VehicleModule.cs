﻿using Ninject.Modules;
using Parking.Core.Interfaces;
using Parking.Core.Repository;
using Parking.Core.RealmM;
using Parking.Core.PlateHelper;
using Parking.Core.Domain.ExitVehicle;


namespace Parking.Core.DependencyResolver
{
    public class VehicleModule : NinjectModule
    {
        public override void Load()
        {
#if DEBUG
            this.Bind<IRepositoryVehicle>().To<VehicleRepositoryMock>().InSingletonScope();
#elif RELEASE
            this.Bind<IRepositoryVehicle>().To<VehicleRepository>().InSingletonScope();
#elif LOCALTEST
            this.Bind<IRepositoryVehicle>().To<VehicleRepositoryMock>().InSingletonScope();
#endif
            this.Bind<ITransactionRequestDBManager>().To<RealmDataManager>().InSingletonScope();
            this.Bind<IPlateValidator>().To<Platevalidator>().InSingletonScope();
            this.Bind<IBussinessLogicVehicleExit>().To<BussinessLogicVehicleExit>().InSingletonScope();
        }

    }
}
