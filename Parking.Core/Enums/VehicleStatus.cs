﻿namespace Parking.Core.Resources.Enums
{
    public enum VehicleStatus
    {
        InsideParkingLot,
        OutParkingLot
    }
}
