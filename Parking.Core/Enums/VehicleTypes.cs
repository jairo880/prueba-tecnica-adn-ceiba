﻿namespace Parking.Core.Resources.Enums
{
    public enum VehicleTypes
    {
        Motorcycle,
        Car
    };
}
