﻿using System;
using System.Collections.Generic;
using Parking.Core.BussinessModel;
using Parking.Core.Interfaces;
using Parking.Core.Utils;
using Parking.Core.DependencyResolver;
using AutoMapper;
using Parking.Core.Utils.AutoMapper;
using Parking.Core.Resources;
using Parking.Core.Resources.Enums;
using Parking.Core.MapperHelper.ManagerMapperService;
using System.Globalization;

namespace Parking.Core
{
    public class BussinessLogicVehicle : IBussinessLogicVehicle
    {
        private IRepositoryVehicle vehicleRepository;
        private readonly IViewVehicle View;
        private IPlateValidator plateValidator;

        public BussinessLogicVehicle()
        {
            //Constructor arguments empty for unit test.
            InitializeInstancesClass();
        }

        public BussinessLogicVehicle(IViewVehicle View)
        {
            InitializeInstancesClass();
            this.View = View;

            MapperService.InitializeAutoMapper();
        }

        private void InitializeInstancesClass()
        {
            this.vehicleRepository = ServiceLocator.Get<IRepositoryVehicle>();
            plateValidator = ServiceLocator.Get<IPlateValidator>();
        }

        public void ValidatePlateEnabledDayToIncomeVehicle(string plate)
        {
            if (ValidatePlateBeginWithACharacter(plate))
            {
                if (DateTimeManager.ValidateDateTimeSundayOrMonday(DateTimeManager.getDateTimeNow()))
                {
                    View.ShowMessageError(ParkingTexts.vehicleNotAuthorizedToIncomeDay);
                    return;
                }
                else
                {
                    ValidateVehicleAlreadyExistOnDB(plate);
                }
            }

            ValidateVehicleAlreadyExistOnDB(plate);
        }

        public Vehicle BuildVehicleToIncomeParking(string plate, VehicleTypes typeVehicle, int engine)
        {
            Vehicle vehicle = new Vehicle
            {
                Plate = plate,
                Type = typeVehicle,
                EngineCC = engine
            };

            vehicle = SetStateVehicleInsideParking(vehicle);
            return vehicle;
        }

        public void AddVehicleDB(Vehicle vehicle)
        {
            if (vehicle == null)
            {
                return;
            }

            List<Vehicle> vehicles = vehicleRepository.GetAllVehiclesRegistered();
            Dictionary<string, int> dictionaryCountVehicles =
                GetQuantityAndTypeVehiclesInsideParkingLot(FilterVehiclesAlreadyParkingLot(vehicles));

            if (!ValidateCapacityParkingLotFull(dictionaryCountVehicles["Car"],
              dictionaryCountVehicles["Motorcycle"], vehicle))
            {
                vehicleRepository.AddVehicleDB(vehicle);
                View.VehicleWasIncomeSuccessfully(ParkingTexts.vehicleWasAddedSuccesfully);
            }
            else
            {
                View.ShowMessageError(ParkingTexts.parkingLotFull);
            }
        }

        public bool ValidatePlateBeginWithACharacter(string plate)
        {
            return plate.ToUpper(CultureInfo.InvariantCulture).StartsWith("A", StringComparison.CurrentCulture);
        }

        private void ValidateVehicleAlreadyExistOnDB(string plate)
        {
            if (vehicleRepository.ValidateVehicleAlreadyExistOnDB(plate))
            {
                PrepareVehicleToUpdateDBRecord(plate);
            }
            else
            {
                View.OnVehicleDontExistOnDatabase(plate);
            }
        }

        private void PrepareVehicleToUpdateDBRecord(string plate)
        {
            UpdateStateVehicleDB(SetStateVehicleInsideParking(GetVehicleByPlate(plate)));
            View.VehicleWasIncomeSuccessfully(ParkingTexts.vehicleWasAddedSuccesfully);
        }

        private void UpdateStateVehicleDB(Vehicle vehicle)
        {
            List<Vehicle> vehicles = vehicleRepository.GetAllVehiclesRegistered();
            Dictionary<string, int> dictionaryCountVehicles =
                GetQuantityAndTypeVehiclesInsideParkingLot(FilterVehiclesAlreadyParkingLot(vehicles));

            if (!ValidateCapacityParkingLotFull(dictionaryCountVehicles["Car"],
                 dictionaryCountVehicles["Motorcycle"], vehicle))
            {
                vehicleRepository.UpdateVehicle(vehicle);
            }
            else
            {
                View.ShowMessageError(ParkingTexts.parkingLotFull);
            }
        }

        private Vehicle GetVehicleByPlate(string plate)
        {
            return vehicleRepository.GetVehicleByPlate(plate);
        }

        public Dictionary<string, int> GetQuantityAndTypeVehiclesInsideParkingLot(List<Vehicle> vehicles)
        {
            int countMotorcycles = 0;
            int countCar = 0;

            foreach (Vehicle vehicle in vehicles)
            {
                if (vehicle.Type == VehicleTypes.Motorcycle)
                {
                    countMotorcycles++;
                }
                else
                {
                    countCar++;
                }
            }

            var dictionary = new Dictionary<string, int>
            {
                { "Car", countCar },
                { "Motorcycle", countMotorcycles }
            };

            return dictionary;

        }

        public bool ValidateCapacityParkingLotFull(int countCar, int countMotorcycle, Vehicle vehicle)
        {
            bool full = false;

            switch (vehicle.Type)
            {
                case VehicleTypes.Motorcycle:
                    full = countMotorcycle >= Convert.ToInt32(ParkingTexts.maxCapacityParkingLotMotorcycles) ?
                        true : false;
                    break;

                case VehicleTypes.Car:
                    full = countCar >= Convert.ToInt32(ParkingTexts.maxCapacityParkingLotCar) ? true : false;
                    break;

                default:
                    full = true;
                    break;
            }

            return full;

        }

        public List<Vehicle> FilterVehiclesAlreadyParkingLot(List<Vehicle> vehicles)
        {
            List<Vehicle> vehiclesAlreadyParkingLot = new List<Vehicle>();

            foreach (Vehicle vehicle in vehicles)
            {
                if (vehicle.Status == VehicleStatus.InsideParkingLot)
                {
                    vehiclesAlreadyParkingLot.Add(vehicle);
                }
            }

            return vehiclesAlreadyParkingLot;
        }

        public Vehicle SetStateVehicleInsideParking(Vehicle vehicle)
        {
            if (vehicle.Status == VehicleStatus.InsideParkingLot)
            {
                return vehicle;
            }

            vehicle.Status = VehicleStatus.InsideParkingLot;
            vehicle.AdmisionVehicleDate = DateTimeManager.getDateTimeNow();
            return vehicle;
        }

        public List<Vehicle> GetVehiclesInParkingLot() => vehicleRepository.GetVehiclesInParkingLot();

        public bool ValidatePlateIsValid(string plate) => plateValidator.ValidatePlateIsValid(plate);
    }
}
