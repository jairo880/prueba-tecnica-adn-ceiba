﻿using System;
using Parking.Core.BussinessModel;
using Parking.Core.Utils;
using Parking.Core.DependencyResolver;
using Parking.Core.Interfaces;
using Parking.Core.Resources;
using Parking.Core.MapperHelper.ManagerMapperService;

namespace Parking.Core.Domain.ExitVehicle
{
    public class BussinessLogicVehicleExit : IBussinessLogicVehicleExit
    {
        private readonly IRepositoryVehicle repositoryVehicle;

        public BussinessLogicVehicleExit()
        {
            repositoryVehicle = ServiceLocator.Get<IRepositoryVehicle>();
            MapperService.InitializeAutoMapper();
        }

        public bool ValidateMotorcycleHasExtraCost(Vehicle vehicle) => vehicle.EngineCC >= Convert.ToInt32(ParkingTexts.topEngineMotorcycleNotExtraValue);

        public Double GetTotalCostToPay(Vehicle vehicle)
        {
            var date = DateTimeManager.GetDifferenceBetweenNowDateAndOtherDate(vehicle.AdmisionVehicleDate);

            Double costToPay = Convert.ToInt32(ParkingTexts.initialCost);

            switch (vehicle.Type)
            {
                case Resources.Enums.VehicleTypes.Motorcycle:
                    if (ValidateMotorcycleHasExtraCost(vehicle))
                    {
                        costToPay += Convert.ToInt32(ParkingTexts.extraValueMotorcycleHigherTopEngine);
                    }
                    costToPay = CalculateMotorcyclePayment(date);
                    break;

                case Resources.Enums.VehicleTypes.Car:
                    costToPay = CalculateCarPayment(date);
                    break;

                default:
                    costToPay = Convert.ToInt32(ParkingTexts.zero);
                    break;
            }

            return costToPay;
        }

        public Double CalculateMotorcyclePayment(TimeSpan date)
        {

            Double costToPay;

            if (date.Hours >= Convert.ToInt32(ParkingTexts.hoursBeginDay) && date.Hours <= Convert.ToInt32(ParkingTexts.hoursFinishDay))
            {
                costToPay = Convert.ToInt32(ParkingTexts.valueDayMotorcycle);
            }
            else if (date.Hours > Convert.ToInt32(ParkingTexts.hoursFinishDay))
            {
                int totalHours = date.Hours / Convert.ToInt32(ParkingTexts.hoursFinishDay);

                costToPay = Math.Ceiling(Double.Parse(totalHours.ToString())) * Convert.ToInt32(ParkingTexts.valueDayMotorcycle);
            }
            else
            {
                costToPay = date.Hours * Convert.ToInt32(ParkingTexts.valueHourMotorcycle);
            }

            if (date.Hours == Convert.ToInt32(ParkingTexts.zero))
            {
                costToPay = Convert.ToInt32(ParkingTexts.initialCost);
            }

            return costToPay;
        }

        public Double CalculateCarPayment(TimeSpan date)
        {

            Double costToPay;

            if (date.Hours >= Convert.ToInt32(ParkingTexts.hoursBeginDay) && date.Hours <= Convert.ToInt32(ParkingTexts.hoursFinishDay))
            {
                costToPay = Convert.ToInt32(ParkingTexts.valueDayCar);
            }
            else if (date.Hours > Convert.ToInt32(ParkingTexts.hoursFinishDay))
            {
                int totalHours = date.Hours / Convert.ToInt32(ParkingTexts.hoursFinishDay);

                costToPay = Math.Ceiling(Double.Parse(totalHours.ToString())) * Convert.ToInt32(ParkingTexts.valueDayCar);
            }
            else
            {
                costToPay = date.Hours * Convert.ToInt32(ParkingTexts.valueHourCar);
            }

            if (date.Hours == Convert.ToInt32(ParkingTexts.zero))
            {
                costToPay = Convert.ToInt32(ParkingTexts.initialCost);
            }

            return costToPay;
        }

        public Vehicle SetStateVehicleOutParking(Vehicle vehicle)
        {
            vehicle.Status = Resources.Enums.VehicleStatus.OutParkingLot;
            return vehicle;
        }

        public void UpdateVehicleDB(Vehicle vehicle)
        {
            repositoryVehicle.UpdateVehicle(vehicle);
        }

        public double GetDaysVehicleInsideParking(Vehicle vehicle)
        {
            double hours = Math.Truncate((DateTimeManager.getDateTimeNow() - vehicle.AdmisionVehicleDate).TotalHours);
            return hours > Convert.ToInt32(ParkingTexts.hoursBeginDay) ? Math.Truncate(hours / Convert.ToInt32(ParkingTexts.hoursBeginDay)) : Convert.ToInt32(ParkingTexts.zero);
        }

        public double GetHoursVehicleInsideParking(Vehicle vehicle) =>
            Math.Truncate((DateTimeManager.getDateTimeNow() - vehicle.AdmisionVehicleDate).TotalHours);

        public void VehicleExit(Vehicle vehicle)
        {
            vehicle = SetStateVehicleOutParking(vehicle);
            UpdateVehicleDB(vehicle);
        }

    }
}