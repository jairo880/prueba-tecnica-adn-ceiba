﻿using AutoMapper;
using Parking.Core.Utils.AutoMapper;

namespace Parking.Core.MapperHelper.ManagerMapperService
{
    public static class MapperService
    {
        public static void InitializeAutoMapper()
        {
            Mapper.Initialize(x =>
            x.AddProfile<MapProfile>()
            );
        }

    }
}
