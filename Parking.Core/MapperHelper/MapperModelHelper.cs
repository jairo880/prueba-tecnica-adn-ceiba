﻿using System.Collections.Generic;
using AutoMapper;
using Parking.Core.BussinessModel;

namespace Parking.Core.Utils
{
    public static class MapperModelHelper
    {
        public static List<Vehicle> MapperListVehicleDBToListVehicleModel(List<VehicleRealmModel> vehicles) =>
            Mapper.Map<List<VehicleRealmModel>, List<Vehicle>>(vehicles);

        public static List<VehicleRealmModel> MapperListVehicleToListVehicleDBModel(List<Vehicle> vehicles) =>
            Mapper.Map<List<Vehicle>, List<VehicleRealmModel>>(vehicles);

        public static Vehicle MapperVehicleDBToVehicleModel(VehicleRealmModel vehicle) =>
            Mapper.Map<VehicleRealmModel, Vehicle>(vehicle);

        public static VehicleRealmModel MapperVehicleToVehicleDBModel(Vehicle vehicle) =>
            Mapper.Map<Vehicle, VehicleRealmModel>(vehicle);

    }
}
