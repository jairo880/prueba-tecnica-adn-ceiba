﻿using AutoMapper;
using Parking.Core.BussinessModel;

namespace Parking.Core.Utils.AutoMapper
{
    internal class MapProfile : Profile
    {
        public void Configure()
        {
            CreateMap<Vehicle, VehicleRealmModel>();
            CreateMap<VehicleRealmModel, Vehicle>();
        }
    }

}
