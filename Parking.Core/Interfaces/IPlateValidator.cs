﻿namespace Parking.Core.Interfaces
{
    public interface IPlateValidator
    {
        bool ValidatePlateIsValid(string plate);
    }
}
