﻿using System;
namespace Parking.Core.Interfaces
{
    public interface IViewVehicle
    {
        void ShowMessageError(string message);

        void OnVehicleDontExistOnDatabase(string plate);

        void VehicleWasIncomeSuccessfully(string message);

    }
}
