﻿using System;
using Parking.Core.BussinessModel;

namespace Parking.Core.Interfaces
{
    public interface IBussinessLogicVehicleExit
    {
        Double GetTotalCostToPay(Vehicle vehicle);

        double GetDaysVehicleInsideParking(Vehicle vehicle);

        double GetHoursVehicleInsideParking(Vehicle vehicle);

        void VehicleExit(Vehicle vehicle);

        Vehicle SetStateVehicleOutParking(Vehicle vehicle);

        bool ValidateMotorcycleHasExtraCost(Vehicle vehicle);

        Double CalculateCarPayment(TimeSpan date);

        Double CalculateMotorcyclePayment(TimeSpan date);

    }
}
