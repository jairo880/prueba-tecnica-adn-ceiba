﻿using Parking.Core.BussinessModel;
using System.Collections.Generic;
using Parking.Core.Resources.Enums;

namespace Parking.Core.Interfaces
{
    public interface IBussinessLogicVehicle
    {
        bool ValidatePlateIsValid(string plate);

        void ValidatePlateEnabledDayToIncomeVehicle(string plate);

        Vehicle BuildVehicleToIncomeParking(string plate, VehicleTypes typeVehicle, int engine);

        void AddVehicleDB(Vehicle vehicle);

        List<Vehicle> GetVehiclesInParkingLot();
    }

}
