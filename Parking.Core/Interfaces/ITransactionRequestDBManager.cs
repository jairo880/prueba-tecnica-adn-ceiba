﻿using System.Collections.Generic;
using Parking.Core.BussinessModel;

namespace Parking.Core.Interfaces
{
    public interface ITransactionRequestDBManager
    {
        List<Vehicle> GetAllVehicles();

        List<Vehicle> GetAllVehiclesCurrentlyParkingLot();

        bool ValidateVehicleAlreadyExistOnDB(string plate);

        Vehicle GetVehicleByPlate(string plate);

        void AddVehicle(Vehicle vehicle);

        void UpdateVehicle(Vehicle vehicle);
    }
}
