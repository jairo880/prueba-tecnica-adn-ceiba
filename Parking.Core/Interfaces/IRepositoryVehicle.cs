﻿using System;
using System.Collections.Generic;
using Parking.Core.BussinessModel;

namespace Parking.Core.Interfaces
{
    public interface IRepositoryVehicle
    {
        bool ValidateVehicleAlreadyExistOnDB(string plate);

        Vehicle GetVehicleByPlate(string plate);

        List<Vehicle> GetAllVehiclesRegistered();

        void AddVehicleDB(Vehicle vehicle);

        void UpdateVehicle(Vehicle vehicle);

        List<Vehicle> GetVehiclesInParkingLot();
    }
}
