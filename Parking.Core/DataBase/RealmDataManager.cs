﻿using System;
using System.Collections.Generic;
using System.Linq;
using Parking.Core.BussinessModel;
using Parking.Core.Interfaces;
using Parking.Core.Utils;
using Realms;

namespace Parking.Core.RealmM
{
    public class RealmDataManager : ITransactionRequestDBManager
    {
        public List<Vehicle> GetAllVehiclesCurrentlyParkingLot()
        {

            var realm = Realm.GetInstance();
            if (realm == null)
            {
                return MapperModelHelper.MapperListVehicleDBToListVehicleModel(new List<VehicleRealmModel>());
            }

            string statusInsideParking = Resources.Enums.VehicleStatus.InsideParkingLot.ToString();

            var vehicles = realm.All<VehicleRealmModel>().Where(vehicle => vehicle.Status == statusInsideParking);
            List<VehicleRealmModel> vehiclesR = vehicles.ToList();

            return MapperModelHelper.MapperListVehicleDBToListVehicleModel(vehiclesR);

        }

        public bool ValidateVehicleAlreadyExistOnDB(string plate)
        {
            bool vehicleWasFound = false;
            var mRealm = Realm.GetInstance();

            var vehicles = mRealm.All<VehicleRealmModel>();

            foreach (VehicleRealmModel vehicle in vehicles)
            {
                if (vehicle.Plate.Equals(plate))
                {
                    vehicleWasFound = true;
                    break;
                }
            }

            return vehicleWasFound;
        }

        public List<Vehicle> GetAllVehicles()
        {
            var realm = Realm.GetInstance();
            if (realm == null)
            {
                return MapperModelHelper.MapperListVehicleDBToListVehicleModel(new List<VehicleRealmModel>());
            }

            var vehicles = realm.All<VehicleRealmModel>();
            List<VehicleRealmModel> vehiclesFinal = vehicles.ToList();

            return MapperModelHelper.MapperListVehicleDBToListVehicleModel(vehiclesFinal);
        }

        public Vehicle GetVehicleByPlate(string plate)
        {
            var realm = Realm.GetInstance();
            var vehicles = realm.All<VehicleRealmModel>().Where(vehicle => vehicle.Plate == plate);
            return MapperModelHelper.MapperVehicleDBToVehicleModel(vehicles.AsEnumerable().ElementAt(0));
        }

        public void AddVehicle(Vehicle vehicle)
        {
            VehicleRealmModel vehicleR = MapperModelHelper.MapperVehicleToVehicleDBModel(vehicle);

            var realm = Realm.GetInstance();
            if (realm == null)
            {
                return;
            }

            realm.Write(() =>
            {
                realm.Add(vehicleR);
            });

        }

        public void UpdateVehicle(Vehicle vehicle)
        {
            var realm = Realm.GetInstance();
            if (realm == null)
            {
                return;
            }

            VehicleRealmModel vehicleR = MapperModelHelper.MapperVehicleToVehicleDBModel(vehicle);
            var vehicles = realm.All<VehicleRealmModel>().Where(item => item.Plate == vehicleR.Plate);

            realm.Write(() =>
            {
                realm.Remove(vehicles.AsEnumerable().ElementAt(0));
                realm.Add(vehicleR);
            });
        }
    }
}






