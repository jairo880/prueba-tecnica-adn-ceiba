﻿using System;
using System.ComponentModel;
using Realms;
using Parking.Core.Resources.Enums;

namespace Parking.Core.BussinessModel
{
    public class VehicleRealmModel : RealmObject
    {
        public string Plate { get; set; }

        public int EngineCC { get; set; }

        public string Status { get; set; }

        public string AdmisionVehicleDate { get; set; }

        public string DepartureVehicleDate { get; set; }

        public string Type { get; set; }

    }
}
