﻿using System;
using System.Text.RegularExpressions;
using Parking.Core.Interfaces;

namespace Parking.Core.PlateHelper
{
    public class Platevalidator : IPlateValidator
    {
        private readonly string regularExpression;

        public Platevalidator()
        {
            regularExpression = "^(?=.*[A-Z])(?=.*[0-9])[A-Z0-9]+$";
        }

        public bool ValidatePlateIsValid(string plate)
        {
            return Regex.IsMatch(plate, regularExpression);
        }
    }

}
