﻿using System;
using Realms;
using Parking.Core.Resources.Enums;

namespace Parking.Core.BussinessModel
{
    public class Vehicle
    {
        public string Plate { get; set; }

        public int EngineCC { get; set; }

        public VehicleStatus Status { get; set; }

        public VehicleTypes Type { get; set; }

        public DateTime AdmisionVehicleDate { get; set; }

        public DateTime DepartureVehicleDate { get; set; }

    }

}