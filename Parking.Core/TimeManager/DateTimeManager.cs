﻿using System;
using System.Globalization;
namespace Parking.Core.Utils
{
    public class DateTimeManager
    {
        public static DateTime getDateTimeNow()
        {
            return DateTime.Now;
        }

        public static bool ValidateDateTimeSundayOrMonday(DateTime dateTime)
        {
            if (dateTime.DayOfWeek == DayOfWeek.Sunday
                || dateTime.DayOfWeek == DayOfWeek.Monday)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static DateTime GetDateFromString(string date) => DateTime.Parse(date);

        public static TimeSpan GetDifferenceBetweenNowDateAndOtherDate(DateTime date) => (DateTimeManager.getDateTimeNow() - date);
    }
}
