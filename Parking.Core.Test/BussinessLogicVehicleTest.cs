﻿using System.Collections.Generic;
using Parking.Core.BussinessModel;
using Xunit;
using System.Linq;
using Parking.Core.DependencyResolver;
using Parking.Core.Repository;
using Parking.Core.Resources.Enums;

namespace Parking.Core.Test
{
    public class BussinessLogicVehicleTest
    {

        private readonly Vehicle vehicle;
        private readonly VehicleRepositoryMock vehicleMock;
        private readonly BussinessLogicVehicle bussinessLogicVehicle;

        public BussinessLogicVehicleTest()
        {
            ServiceLocator.ConfigureKernel();

            vehicleMock = new VehicleRepositoryMock();
            bussinessLogicVehicle = ServiceLocator.Get<BussinessLogicVehicle>();
            vehicle = vehicleMock.GetAllVehiclesRegistered().ElementAt(0);

        }

        [Fact]
        public void PlateBeginWithACharacterTest()
        {

            //Arrange
            vehicle.Plate = "ABC236";

            //Act
            bool result = bussinessLogicVehicle.ValidatePlateBeginWithACharacter(vehicle.Plate);


            //Assert
            Assert.True(result);
        }

        [Fact]
        public void PlateBeginWithACharacterTest2()
        {

            //Arrange
            vehicle.Plate = "AFD221";

            //Act
            bool result = bussinessLogicVehicle.ValidatePlateBeginWithACharacter(vehicle.Plate);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void PlateBeginWithACharacterTest3()
        {

            //Arrange
            vehicle.Plate = "CBC236";

            //Act
            bool result = bussinessLogicVehicle.ValidatePlateBeginWithACharacter(vehicle.Plate);

            //Assert
            Assert.False(result);
        }


        [Fact]
        public void GetQuantityAndTypeVehiclesInsideParkingLotTest()
        {
            //Arrange
            List<Vehicle> vehicles = vehicleMock.GetAllVehiclesRegistered();

            //Act
            Dictionary<string, int> dictionary = bussinessLogicVehicle.GetQuantityAndTypeVehiclesInsideParkingLot(vehicles);
            int countCarExpected = 1;
            int countMotorcycleExpected = 0;

            //Assert
            Assert.Equal(dictionary["Car"], countCarExpected);
            Assert.Equal(dictionary["Motorcycle"], countMotorcycleExpected);
        }

        [Fact]
        public void FilterVehiclesAlreadyParkingLotTest2()
        {
            //Arrange
            List<Vehicle> vehiclesFiltered = new List<Vehicle>();
            List<Vehicle> vehicles = vehicleMock.GetAllVehiclesRegistered();

            //Act
            vehicleMock.AddVehicleDB(new Vehicle
            {
                Plate = "SDF323",
                Type = VehicleTypes.Motorcycle,
                EngineCC = 200,
                Status = VehicleStatus.InsideParkingLot
            });
            vehiclesFiltered = bussinessLogicVehicle.FilterVehiclesAlreadyParkingLot(vehicles);

            //Assert
            Assert.NotEmpty(vehiclesFiltered);
        }

        [Fact]
        public void FilterVehiclesAlreadyParkingLotTest3()
        {
            //Arrange
            List<Vehicle> vehiclesFiltered = new List<Vehicle>();

            //Act
            vehicleMock.AddVehicleDB(new Vehicle
            {
                Plate = "SDF323",
                Type = VehicleTypes.Motorcycle,
                EngineCC = 200,
                Status = VehicleStatus.InsideParkingLot
            });

            List<Vehicle> vehicles = vehicleMock.GetAllVehiclesRegistered();
            vehiclesFiltered = bussinessLogicVehicle.FilterVehiclesAlreadyParkingLot(vehicles);

            //Assert
            Assert.NotNull(vehiclesFiltered);
        }

        [Fact]
        public void SetStateVechicleInsideParkingTest()
        {
            //Arrange
            Vehicle vehicleResult;
            VehicleStatus expectedStatusVehicle = VehicleStatus.InsideParkingLot;

            //Act
            vehicleResult = bussinessLogicVehicle.SetStateVehicleInsideParking(vehicleMock.GetAllVehiclesRegistered().ElementAt(0));

            //Assert
            Assert.Equal(vehicleResult.Status.ToString(), expectedStatusVehicle.ToString());
        }

        [Fact]
        public void SetStateVechicleInsideParkingTest2()
        {
            //Arrange
            Vehicle vehicleResult;

            //Act
            vehicleResult = bussinessLogicVehicle.SetStateVehicleInsideParking(vehicleMock.GetAllVehiclesRegistered().ElementAt(0));

            //Assert
            Assert.NotNull(vehicleResult);
        }


        [Fact]
        public void ValidateCapacityParkingLotFullTest()
        {
            //Arrange
            int currentCountCarParking = 20;
            int currentCountMotorcycleParking = 5;
            Vehicle vehicleM = vehicleMock.GetAllVehiclesRegistered().ElementAt(0);

            //Act
            bool resultParkingLot = bussinessLogicVehicle.
                ValidateCapacityParkingLotFull(currentCountCarParking, currentCountMotorcycleParking, vehicleM);

            //Assert
            Assert.True(resultParkingLot);
        }

        [Fact]
        public void ValidateCapacityParkingLotFullTest2()
        {
            //Arrange
            int currentCountCarParking = 19;
            int currentCountMotorcycleParking = 5;
            Vehicle vehicleM = vehicleMock.GetAllVehiclesRegistered().ElementAt(0);

            //Act
            bool resultParkingLot = bussinessLogicVehicle.
                ValidateCapacityParkingLotFull(currentCountCarParking, currentCountMotorcycleParking, vehicleM);

            //Assert
            Assert.False(resultParkingLot);
        }

        [Fact]
        public void BuildVehicleToIncomeParkingTest()
        {
            //Arrange
            string plate = "ABC236";
            VehicleTypes typeVehicle = VehicleTypes.Car;
            int engine = 2000;

            //Act
            Vehicle tempVehicle = bussinessLogicVehicle.BuildVehicleToIncomeParking(plate, typeVehicle, engine);

            //Assert
            Assert.NotNull(tempVehicle);
        }

        [Fact]
        public void BuildVehicleToIncomeParkingTest2()
        {
            //Arrange
            string plate = "ABC237";
            VehicleTypes typeVehicle = VehicleTypes.Car;
            int engine = 2000;

            //Act
            Vehicle tempVehicle = bussinessLogicVehicle.BuildVehicleToIncomeParking(plate, typeVehicle, engine);

            //Assert
            Assert.Equal(plate, tempVehicle.Plate);
        }

        [Fact]
        public void BuildVehicleToIncomeParkingTest3()
        {
            //Arrange
            string plate = "ABC237";
            VehicleTypes typeVehicle = VehicleTypes.Car;
            int engine = 1600;

            //Act
            Vehicle tempVehicle = bussinessLogicVehicle.BuildVehicleToIncomeParking(plate, typeVehicle, engine);

            //Assert
            Assert.Equal(engine, tempVehicle.EngineCC);
        }

        [Fact]
        public void BuildVehicleToIncomeParkingTest4()
        {
            //Arrange
            string plate = "ABC237";
            VehicleTypes typeVehicle = VehicleTypes.Car;
            int engine = 1600;

            //Act
            Vehicle tempVehicle = bussinessLogicVehicle.BuildVehicleToIncomeParking(plate, typeVehicle, engine);

            //Assert
            Assert.Equal(typeVehicle, tempVehicle.Type);
        }
    }
}
