﻿using Parking.Core.DependencyResolver;
using Parking.Core.Interfaces;
using Parking.Core.BussinessModel;
using Xunit;
using System.Linq;
using Parking.Core.Repository;

namespace Parking.Core.Test
{
    public class PlateValidatorTest
    {
        private readonly IPlateValidator plateValidator;
        private readonly VehicleRepositoryMock vehicleRepositoryMock;
        private readonly Vehicle vehicle;

        public PlateValidatorTest()
        {
            ServiceLocator.ConfigureKernel();
            plateValidator = ServiceLocator.Get<IPlateValidator>();
            vehicleRepositoryMock = new VehicleRepositoryMock();
            vehicle = vehicleRepositoryMock.GetAllVehiclesRegistered().ElementAt(0);
        }

        [Fact]
        public void ValidatePlateTest()
        {

            //Arrange
            vehicle.Plate = "ABC236";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void ValidatePlateTest1()
        {

            //Arrange
            vehicle.Plate = "ABCABC";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidatePlateTest2()
        {

            //Arrange
            vehicle.Plate = "11111";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidatePlateTest3()
        {

            //Arrange
            vehicle.Plate = "!ABC236";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidatePlateTest4()
        {

            //Arrange
            vehicle.Plate = "!ABC236";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidatePlateTest5()
        {

            //Arrange
            vehicle.Plate = "ABC236!";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidatePlateTest6()
        {

            //Arrange
            vehicle.Plate = "ABC_236";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidatePlateTest7()
        {

            //Arrange
            vehicle.Plate = "WER351";

            //Act
            bool result = plateValidator.ValidatePlateIsValid(vehicle.Plate);

            //Assert
            Assert.True(result);
        }
    }
}
