﻿using Parking.Core.BussinessModel;
using Xunit;
using System.Linq;
using Parking.Core.DependencyResolver;
using Parking.Core.Resources.Enums;
using Parking.Core.Interfaces;
using Parking.Core.Repository;
using System;
using Parking.Core.Resources;
using Parking.Core.Utils;

namespace Parking.Core.Test
{
    public class BussinessLogicVehicleExitTest
    {

        private readonly Vehicle vehicle;
        private readonly IBussinessLogicVehicleExit bussinessLogicVehcleExit;
        private readonly VehicleRepositoryMock vehicleRepositoryMock;

        public BussinessLogicVehicleExitTest()
        {
            ServiceLocator.ConfigureKernel();
            bussinessLogicVehcleExit = ServiceLocator.Get<IBussinessLogicVehicleExit>();
            vehicleRepositoryMock = new VehicleRepositoryMock();
            vehicle = vehicleRepositoryMock.GetAllVehiclesRegistered().ElementAt(0);
        }

        [Fact]
        public void ValidateMotorcycleHasExtraCostTest()
        {
            //Arrange
            vehicle.EngineCC = 600;

            vehicle.Type = VehicleTypes.Motorcycle;

            //Act
            bool result = bussinessLogicVehcleExit.ValidateMotorcycleHasExtraCost(vehicle);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void ValidateMotorcycleHasExtraCost2Test()
        {
            //Arrange
            vehicle.EngineCC = 100;
            vehicle.Type = VehicleTypes.Motorcycle;

            //Act
            bool result = bussinessLogicVehcleExit.ValidateMotorcycleHasExtraCost(vehicle);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void ValidateMotorcycleHasExtraCost3Test()
        {
            //Arrange
            vehicle.Type = VehicleTypes.Motorcycle;
            vehicle.EngineCC = 500;

            //Act
            bool result = bussinessLogicVehcleExit.ValidateMotorcycleHasExtraCost(vehicle);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void SetStateVechicleOutParkingTest()
        {
            //Arrange
            Vehicle vehicleResult;
            VehicleStatus expectedStatusVehicle = VehicleStatus.OutParkingLot;
            vehicle.Status = VehicleStatus.OutParkingLot;

            //Act
            vehicleResult = bussinessLogicVehcleExit.SetStateVehicleOutParking(vehicle);

            //Assert
            Assert.Equal(vehicleResult.Status, expectedStatusVehicle);
        }

        [Fact]
        public void SetStateVechicleOutParkingTest1()
        {
            //Arrange
            Vehicle vehicleResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;

            //Act
            vehicleResult = bussinessLogicVehcleExit.SetStateVehicleOutParking(vehicle);

            //Assert
            Assert.NotNull(vehicleResult);
        }

        [Fact]
        public void GetHoursVehicleInsideParkingTest()
        {
            //Arrange
            double hoursResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;

            //Act
            hoursResult = bussinessLogicVehcleExit.GetHoursVehicleInsideParking(vehicle);

            //Assert
            Assert.NotNull(hoursResult);
        }

        [Fact]
        public void GetHoursVehicleInsideParkingTest1()
        {
            //Arrange
            double hoursResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;

            //Act
            hoursResult = bussinessLogicVehcleExit.GetHoursVehicleInsideParking(vehicle);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), hoursResult);
        }

        [Fact]
        public void GetDaysVehicleInsideParkingTest()
        {
            //Arrange
            double daysResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;

            //Act
            daysResult = bussinessLogicVehcleExit.GetDaysVehicleInsideParking(vehicle);

            //Assert
            Assert.NotNull(daysResult);
        }

        [Fact]
        public void GetDaysVehicleInsideParkingTest1()
        {
            //Arrange
            double daysResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;

            //Act
            daysResult = bussinessLogicVehcleExit.GetHoursVehicleInsideParking(vehicle);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), daysResult);
        }


        [Fact]
        public void CalculateCarPaymentTest()
        {
            //Arrange
            double costToPayResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;
            vehicle.Type = VehicleTypes.Car;

            //Act
            var date = DateTimeManager.GetDifferenceBetweenNowDateAndOtherDate(vehicle.AdmisionVehicleDate);
            costToPayResult = bussinessLogicVehcleExit.CalculateCarPayment(date);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), costToPayResult);
        }


        [Fact]
        public void CalculateMotorcyclePaymentTest()
        {
            //Arrange
            double costToPayResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;
            vehicle.Type = VehicleTypes.Motorcycle;

            //Act
            var date = DateTimeManager.GetDifferenceBetweenNowDateAndOtherDate(vehicle.AdmisionVehicleDate);
            costToPayResult = bussinessLogicVehcleExit.CalculateMotorcyclePayment(date);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), costToPayResult);
        }

        [Fact]
        public void CalculateMotorcyclePayment()
        {
            //Arrange
            double costToPayResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;
            vehicle.Type = VehicleTypes.Motorcycle;

            //Act
            var date = DateTimeManager.GetDifferenceBetweenNowDateAndOtherDate(vehicle.AdmisionVehicleDate);
            costToPayResult = bussinessLogicVehcleExit.CalculateMotorcyclePayment(date);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), costToPayResult);
        }

        [Fact]
        public void GetTotalCostToPayMotorcycleTest()
        {
            //Arrange
            double costToPayResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;
            vehicle.Type = VehicleTypes.Motorcycle;

            //Act
            costToPayResult = bussinessLogicVehcleExit.GetTotalCostToPay(vehicle);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), costToPayResult);
        }

        [Fact]
        public void GetTotalCostToPayCarTest()
        {
            //Arrange
            double costToPayResult;
            vehicle.Status = VehicleStatus.InsideParkingLot;
            vehicle.Type = VehicleTypes.Car;

            //Act
            costToPayResult = bussinessLogicVehcleExit.GetTotalCostToPay(vehicle);

            //Assert
            Assert.NotEqual(Convert.ToInt32(ParkingTexts.zero), costToPayResult);
        }

    }
}
