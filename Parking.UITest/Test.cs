﻿using System;
using System.IO;
using NUnit.Framework;
using Xamarin.UITest;

namespace Parking.UITest
{
    [TestFixture(Platform.Android)]
    public class Test
    {
        IApp app;
        readonly Platform platform;
        readonly string vehiclePlate;
        readonly string messageVehicleAddedSuccessfully;
        readonly string messageVehicleOutSuccessfully;


        public Test(Platform platform)
        {
            this.platform = platform;
            vehiclePlate = "VLC232";
            messageVehicleAddedSuccessfully = "¡El vehículo fué agregado exitosamente!";
            messageVehicleOutSuccessfully = "Salida del vehículo exitosa.";

        }

        [SetUp]
        public void BeforeEachTest()
        {
            string path = Directory.GetCurrentDirectory();
            Console.WriteLine("Directorio" + path);

            app = ConfigureApp.Android.ApkFile("../../../Parking.Droid/bin/Release/com.ceibasoftware.parking.apk").StartApp();
        }

        [Test]
        public void AppLaunches()
        {
            Assert.NotNull(app);
        }

        [Test]
        public void ShowAlertDialogPlateVehicle()
        {
            //Act
            ShowVehicleDialog();

            //Assert
            app.WaitForElement(c => c.Marked("Placa vehículo"));
        }

        [Test]
        public void ShowAlertDialogPlateNewVehicle()
        {
            //Arrange
            ShowVehicleDialog();

            //Act
            app.EnterText(x => x.Id("edit_text_plate"), vehiclePlate);
            app.Back();
            app.Tap(x => x.Id("button_income_vehicle_dialog"));

            //Assert
            app.WaitForElement(c => c.Marked("Nuevo vehículo"));
        }

        [Test]
        public void AddNewVehicle()
        {
            //Arrange
            ShowAlertDialogPlateNewVehicle();

            //Act
            app.Tap(view => view.Id("spinner_type_new_vehicle_dialog")); // open the spinner control with the id: "spinner"
            app.Tap(view => view.Text("Carro"));
            app.EnterText(view => view.Id("edit_text_engine_new_vehicle_dialog"), "1200");
            app.Back();
            app.Tap(c => c.Id("button_income_dialog_new_vehicle"));

            //Assert
            app.WaitForElement(c => c.Marked(messageVehicleAddedSuccessfully));

        }

        [Test]
        public void ViewDetailVehicleInParking()
        {
            //Arrange
            AddNewVehicle();

            //Act
            app.Tap(view => view.Text(vehiclePlate));

            //Assert
            app.WaitForElement(c => c.Id("button_out_vehicle"));

        }

        [Test]
        public void OutVehicleParking()
        {
            //Arrange
            ViewDetailVehicleInParking();

            //Act
            app.Tap(view => view.Id("button_out_vehicle"));

            //Assert
            app.WaitForElement(c => c.Text(messageVehicleOutSuccessfully));
        }


        #region CommonFlow

        private void ShowVehicleDialog()
        {
            app.Tap(x => x.ClassFull("Refractored.Fab.FloatingActionButton"));
        }

        #endregion

    }
}
